const express = require("express");
const router = express.Router();
const todoController = require("../controllers/todoController");


//<<-------------REGISTER---------------->>\\
router.post("/addToDoList", (req, res) => {
	todoController.createToDoList(req.body).then(resultFromController => res.send(resultFromController));
});


router.delete("/:todoId/delete", (req, res) => {
	todoController.deleteToDoList(req.params).then(resultFromController => res.send(resultFromController));
});


router.get("/", (req, res) => {
	todoController.viewAllToDoLists().then(resultFromController => res.send(resultFromController));
});


router.patch("/:todoId/updateToDoList",  (req, res) => {
 	const data = {
		reqBody: req.body,
		reqParams: req.params
	}
 	todoController.updateToDoList(data).then(resultFromController => res.send(resultFromController));
 });




module.exports = router;





































/*router.get("/:userId/orders",auth.verify, (req, res) => {
	userController.getOrders(req.params).then(resultFromController => res.send(resultFromController));
});

router.get("/allOrders", (req, res) => {
	const data = {
		reqOrder: {},
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.getAllOrders(data).then(resultFromController => res.send(resultFromController));
});*/

/*router.post("/checkout",auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id, 
		bookId: req.body.bookId
	}
	userController.checkout(data).then(resultFromController => res.send(resultFromController))
})
*/



