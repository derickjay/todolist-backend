const ToDoList = require("../models/ToDoList");


module.exports.createToDoList = async (reqBody) => {

  const newList = new ToDoList({
    title: reqBody.title
  });
  return newList.save().then((user, error) => {
    if (error) {
      return { "inputError": true };
    } else {
      return true;
    };
  });
};


module.exports.deleteToDoList = async (reqParams) => {
	  const deletedList = await ToDoList.findByIdAndDelete(reqParams.todoId);
	  if (!deletedList) {
		return { "notFoundError": true };
	  }
	  return true;
  };

  module.exports.viewAllToDoLists = async () => {
	try {
	  const lists = await ToDoList.find({});
	  return lists;
	} catch (error) {
	  return { "serverError": true };
	}
  };

  module.exports.updateToDoList = async (data) => {
	const updates = {
	  title: data.reqBody.title
	};
	try {
	  const updatedList = await ToDoList.findByIdAndUpdate(data.reqParams.todoId, updates, { new: true });
	  if (!updatedList) {
		return { "notFoundError": true };
	  }
	  return updatedList;
	} catch (error) {
	  return { "serverError": true };
	}
  };

