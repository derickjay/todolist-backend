const mongoose = require("mongoose");

const toDoListSchema = new mongoose.Schema({
title: {
  type: String,
  required: [true, "Title is required"]
}
});

module.exports = mongoose.model("ToDoList", toDoListSchema);

