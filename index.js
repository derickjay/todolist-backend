const express = require("express");
// Mongoose is a package that allows creating of Schemas to our model our data structures
// Also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

// Allows access to routes defined within our application
const todoRoute = require("./routes/todoRoute")
// const orderRoute = require("./routes/orderRoute")

// Create an application using express
const app = express();

// Middleware
// Allows all resources to access our backend application
app.use(cors());
// Allows your app to read json data
app.use(express.json());
// Allows your app to read data from forms
// {extended:true} allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));

// Database Connection
mongoose.connect("mongodb+srv://derickjayyy12345:admin123@zuitt-bootcamp.mx8pwpr.mongodb.net/ToDoList?retryWrites=true&w=majority", 
	{ 
		// Due to updates in Mongo DB drivers that allow connection to it, the default connection string is being flagged as an error
		// By default a warning will be displayed in the terminal when the application is run, but this will not prevent Mongoose from being used in the application
		// { newUrlParser : true } allows us to avoid any current and future errors while connecting to Mongo DB
		useNewUrlParser : true,  
		useUnifiedTopology : true
	}
);
mongoose.connection.once("open", () => console.log('Now connected to the database!'));



// Defines the "/courses" string to be included for all user routes defined in the "course" route file
app.use("/todos",todoRoute);

// Server listening
// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`));


